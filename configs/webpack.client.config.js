const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const MODE = process.env.MODE ?? 'development';
const IS_DEV = MODE === 'development';
const IS_PROD = MODE === 'production';

const CSS_GLOBAL_REGEX = /\.global.css$/;

module.exports = {
    mode: MODE,
    entry: [path.join(__dirname, '../src/client/index.tsx')],
    output: {
        path: path.join(__dirname, '../build/client'),
        filename: 'index.js',
        publicPath: '/static/',
        clean: true,
    },
    plugins: IS_DEV ? [new CleanWebpackPlugin()] : [],
    module: {
        rules: [
            {
                test: /\.[tj]sx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: path.resolve(__dirname, '../tsconfig.json'),
                        },
                    },
                ],
            },
            {
                test: CSS_GLOBAL_REGEX,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.css$/,
                exclude: CSS_GLOBAL_REGEX,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                        },
                    },
                ],
            },
            // {
            //     test: /\.svg$/,
            //     use: 'svg-inline-loader',
            // },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            // publicPath: '/static',
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
        alias: {
            '@pictures': path.resolve(__dirname, '../src/pictures'),
            // 'react-dom': IS_DEV ? '@hot-loader/react-dom' : 'react-dom',
        },
    },
};
