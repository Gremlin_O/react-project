const nodeExternals = require('webpack-node-externals');

const path = require('path');
const MODE = process.env.MODE ?? 'development';
const IS_DEV = MODE === 'development';
const IS_PROD = MODE === 'production';

const CSS_GLOBAL_REGEX = /\.global.css$/;

module.exports = {
    target: 'node',
    mode: MODE,
    entry: path.join(__dirname, '../src/server/index.tsx'),
    output: {
        path: path.join(__dirname, '../build/server/'),
        filename: 'index.js',
        clean: true,
    },
    module: {
        rules: [
            {
                test: /\.[tj]sx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: path.resolve(__dirname, '../tsconfig.json'),
                        },
                    },
                ],
            },
            {
                test: CSS_GLOBAL_REGEX,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.css$/,
                exclude: CSS_GLOBAL_REGEX,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
        alias: {
            '@pictures': path.resolve(__dirname, '../src/pictures'),
        },
    },
    externals: [nodeExternals()],
    optimization: {
        minimize: false,
    },
};
