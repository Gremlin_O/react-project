export const generateHTMLTemplate = (content: string) => `
<!DOCTYPE html>

<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reddit</title>
    <script src="/static/index.js" defer></script>
</head>

<body>
    <div id="react-root">${content}</div>
</body>

</html>`;
