import express from 'express';
import reactDOM from 'react-dom/server';
import App from '../shared/App';
import { generateHTMLTemplate } from './templateGenerator';
import React from 'react';

const app = express();
const PORT = process.env.PORT ?? 3000;

app.use('/static', express.static('./build/client'));

app.get('/', (req, res) => {
	res.contentType('text/html');
	res.status(200).send(generateHTMLTemplate(reactDOM.renderToString(<App />)));
});

app.listen(PORT, () => {
	console.log(`Server is started on port ${PORT}`);
});
