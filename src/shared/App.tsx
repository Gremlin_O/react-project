import React, { useState } from 'react';
import styles from './styles.css';
import Layout from './Layout/Layout';
import Header from './Header/Header';
import Content from './Content/Content';

import './main.global.css';
import CardsList from './CardsList/CardsList';

const App = () => {
    return (
        <Layout>
            <Header />
            <Content>
                <CardsList />
            </Content>
        </Layout>
    );
};

export default App;
