import React from 'react';
import styles from './MenuButton.css';
import menuBtn from '@pictures/menuBtn.svg';

const MenuButton = () => {
    return (
        <span className={styles.menuBtnCont}>
            <span
                className={styles.menuBtn}
                style={{ backgroundImage: `url(/static/${menuBtn})` }}
            ></span>
        </span>
    );
};

export default MenuButton;
