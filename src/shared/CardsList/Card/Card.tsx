import React from 'react';
import styles from './Card.css';
import CardHeader from './CardHeader/CardHeader';
import MenuButton from './MenuButton/MenuButton';
import CardPreview from './CardPreview/CardPreview';
import MenuBar from './MenuBar/MenuBar';

const Card = () => {
    return (
        <div className={styles.card}>
            <CardHeader />
            <CardPreview />
            <MenuBar />
            <MenuButton />
        </div>
    );
};

export default Card;
