import React from 'react';
import styles from './CardHeader.css';
import AuthorBar from './AuthorBar/AuthorBar';

const CardHeader = () => {
    return (
        <div className={styles.cardHeader}>
            <AuthorBar />
            <h1>Реплицированные с зарубежных источников возможности</h1>
        </div>
    );
};

export default CardHeader;
