import React from 'react';
import styles from './AuthorBar.css';
import feedImg from '@pictures/authorImg.png';

const AuthorBar = () => {
    return (
        <div className={styles.authorBar}>
            <img src={'static/' + feedImg} alt='#' />
            <h1 className={styles.authorName}>Дмитрий Гришин</h1>
            <h1 className={styles.timeText}>
                <span>опубликовано </span>8 часов назад
            </h1>
        </div>
    );
};

export default AuthorBar;
