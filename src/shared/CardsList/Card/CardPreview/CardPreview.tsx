import React from 'react';
import styles from './CardPreview.css';
import previewImg from '@pictures/previewImg.png';

const CardPreview = () => {
    const imgStyle = {
        backgroundImage: `url(/static/${previewImg})`,
    };
    return (
        <div className={styles.preview}>
            <div style={imgStyle} className={styles.previewImg} />
        </div>
    );
};

export default CardPreview;
