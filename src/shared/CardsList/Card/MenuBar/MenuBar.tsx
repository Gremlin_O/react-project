import React from 'react';
import styles from './MenuBar.css';
import arrowImg from '@pictures/arrowBtn.svg';
import commImg from '@pictures/commImg.svg';
import shareImg from '@pictures/shareBtn.svg';
import addImg from '@pictures/addBtn.svg';

const MenuBar = () => {
    return (
        <div className={styles.menuBar}>
            <div className={styles.carmaCont}>
                <svg width='19' height='10' viewBox='0 0 19 10' xmlns='http://www.w3.org/2000/svg'>
                    <path d='M9.5 0L0 10H19L9.5 0Z' />
                </svg>

                <h1>101</h1>
                <svg width='19' height='10' viewBox='0 0 19 10' xmlns='http://www.w3.org/2000/svg'>
                    <path d='M9.5 0L0 10H19L9.5 0Z' />
                </svg>
            </div>
            <div className={styles.commCont}>
                <span style={{ background: `url(/static/${commImg})` }}></span>
                <h1>14</h1>
            </div>
            <div className={styles.btnCont}>
                <span style={{ background: `url(/static/${shareImg})` }}></span>
                <span style={{ background: `url(/static/${addImg})` }}></span>
            </div>
        </div>
    );
};

export default MenuBar;
