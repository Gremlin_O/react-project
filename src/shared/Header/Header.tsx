import React from 'react';
import SearchBlock from './SearchBlock/SearchBlock';
import ThreadTitle from './ThreadTitle/ThreadTitle';
import SortBlock from './SortBlock/SortBlock';
import styles from './Header.css';

const Header = () => {
    return (
        <header className={styles.header}>
            <SearchBlock />
            <ThreadTitle />
            <SortBlock />
        </header>
    );
};

export default Header;
