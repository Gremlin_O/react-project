import React from 'react';
import styles from './SortBlock.css';
import rocketImg from '@pictures/rocketImg.svg';
import arrowLogo from '@pictures/arrowImage.svg';

const SortBlock = () => {
    return (
        <div className={styles.sortBlock}>
            <div className={styles.sortingCont}>
                <img className={styles.penImage} src={'static/' + rocketImg} alt='#' />
                <p className={styles.sortText}>Лучшие</p>
            </div>
            <img src={'static/' + arrowLogo} alt='#' />
        </div>
    );
};

export default SortBlock;
