import React from 'react';
import styles from './Threadtitle.css';

const ThreadTitle = () => {
    return <div className={styles.threadTitle}>Дискуссии</div>;
};

export default ThreadTitle;
