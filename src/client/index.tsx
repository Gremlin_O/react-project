import { hydrateRoot } from 'react-dom/client';
import React from 'react';
import App from '../shared/App';

const rootNode = document.getElementById('react-root') as HTMLElement;
hydrateRoot(rootNode, <App />);
