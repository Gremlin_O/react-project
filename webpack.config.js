const clientConfig = require('./configs/webpack.client.config.js');
const serverConfig = require('./configs/webpack.server.config.js');

module.exports = [clientConfig, serverConfig];
