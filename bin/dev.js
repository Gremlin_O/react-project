const webpack = require('webpack');
const compilerOptions = require('../webpack.config');
const nodemon = require('nodemon');
const path = require('path');

const MODE = process.env.MODE;
const IS_DEV = MODE === 'development';

const compiler = webpack(compilerOptions);

const SERVER_BUILD_PATH = path.resolve(__dirname, '../build/server/index.js');

compiler.run((err) => {
	if (err) return console.error('Compilation error', err);

	console.log('Compilation succeded');

	compiler.watch({}, (err) => {
		if (err) return console.error('Watch error', err);
		console.log('Recompiled (by watch)');
	});

	nodemon({
		script: SERVER_BUILD_PATH,
	});
});
